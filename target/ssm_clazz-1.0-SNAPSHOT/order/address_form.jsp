<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>生成订单 - 当当网</title>
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/page_bottom.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/address.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<%@include file="../common/head1.jsp"%>
	<div class="login_step">
		生成订单骤: 1.确认订单 ><span class="red_bold"> 2.填写送货地址</span> > 3.订单成功
	</div>
	<div class="fill_message">
		 
		 <form action="${pageContext.request.contextPath}/order/addOrder" method="post">
			 <div class="address-wrap">
				<ul class="address-list">
					<c:forEach items="${sessionScope.addresses}" var="add">
						<c:if test="${add.select_state!=0}">
							<li class="selected">
								<div class="addressBox" id="d">
									<span class="marker-tip">寄送至</span>
								</div>
								<label class="addressInfo">
									<input type="radio" name="addressId" value="${add.id}" onclick="changeState(this)"/>
									<span class="user-address">
								<span >${add.address}</span>
								<span>（</span>
								<span>${add.name}</span>
								<span> 收）</span>
								<em>${add.mobile}</em>
							</span>
								</label>
							</li>
						</c:if>
						<c:if test="${add.select_state==0}">
							<li>
								<label class="addressInfo">
									<input type="radio" name="addressId" value="${add.id}" onclick="changeState(this)"/>
									<span class="user-address">
								<span>${add.address}</span>
								<span>（</span>
								<span>${add.name}</span>
								<span> 收）</span>
								<em>${add.mobile}</em>
							</span>
								</label>
							</li>
						</c:if>
					</c:forEach>
				</ul>
			</div>
			
			<div class="login_in">
				<input
					id="btnClientRegister" class="button_1" name="submit" type="submit"
					value="下一步" />
			</div>
			
		</form>
	</div>
	<%@include file="../common/foot1.jsp"%>
	<script type="text/javascript">
		function  changeState(obj){
			/*console.log(obj.parentNode);*/
			/* 改变没有选中的地址背景 */
			let liArr = document.getElementsByTagName("li");
			for (let i = 0; i<liArr.length; i++) {
					liArr[i].className="";
			}
			/* 改变选中的地址的背景 */
			obj.parentNode.parentNode.className="selected";

			/* 没有选中的地址前去掉寄送至 */
			document.getElementById("d").parentNode.removeChild(document.getElementById("d"));

			/* 选中的地址前加上寄送至 */
			let text = "<div class='addressBox' id='d'><span class='marker-tip'>寄送至</span></div>";
			obj.parentNode.insertAdjacentHTML("beforebegin",text);
		}
	</script>
	
</body>
</html>