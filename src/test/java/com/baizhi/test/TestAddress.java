package com.baizhi.test;

import com.baizhi.entity.Address;
import com.baizhi.entity.User;
import com.baizhi.mapper.AddressMapper;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class TestAddress {
    @Test
    public void Insert(){
        User user = new User();
        user.setId(6);
        Address address = new Address(null,"lin","ddd","8852",user,1,0);
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AddressMapper mapper = ctx.getBean("addressMapper", AddressMapper.class);
        mapper.insertAddressById(address);
    }
    @Test
    public void update(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AddressMapper mapper = ctx.getBean("addressMapper", AddressMapper.class);
        List<Address> addresses = mapper.selectAllAddressById(6);
        System.out.println("addresses = " + addresses);
    }
    @Test
    public void updateDelect(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AddressMapper mapper = ctx.getBean("addressMapper", AddressMapper.class);
        mapper.updateDeleteStateToZero(6,1);
    }
    @Test
    public void updateSelect(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AddressMapper mapper = ctx.getBean("addressMapper", AddressMapper.class);
        mapper.updateSelectStateToZeroById(6,1);
    }
    @Test
    public void updateSelectToZero(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AddressMapper mapper = ctx.getBean("addressMapper", AddressMapper.class);
        mapper.updateSelectStateToOneById(6,1);
    }
    @Test
    public void selectSelectAddress(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AddressMapper mapper = ctx.getBean("addressMapper", AddressMapper.class);
        Address address = mapper.selectSelectAddressById(6);
        System.out.println("address = " + address);
    }
}
