package com.baizhi.test;

import com.baizhi.entity.Category;
import com.baizhi.mapper.CategoryMapper;
import com.baizhi.service.CategoryService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class TestCategory {
    @Test
    public void selectAll(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        CategoryMapper mapper = ctx.getBean("categoryMapper", CategoryMapper.class);
        List<Category> list = mapper.selectAllCategory();
        for (Category category : list) {
//            System.out.println(category.getName());
//            for (Category categoryCategory : category.getCategories()) {
//                System.out.println(categoryCategory.getName());
//            }
            System.out.println(category);
        }
    }
    @Test
    public void queryAll(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        CategoryService categoryService = ctx.getBean("categoryServiceImpl", CategoryService.class);
//        List<Category> categories = categoryService.queryAllCateGory();
//        System.out.println(categories);
        Integer integer = categoryService.queryCountByParentId(1);
        System.out.println("integer = " + integer);
    }
    @Test
    public void queryNameById(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        CategoryMapper mapper = ctx.getBean("categoryMapper", CategoryMapper.class);
        String name = mapper.selectCateNameByParentId(1);
        System.out.println("name = " + name);
    }
    @Test
    public void queryCount(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        CategoryMapper mapper = ctx.getBean("categoryMapper", CategoryMapper.class);
        Integer count = mapper.selectCountByParentId(1);
        System.out.println("count = " + count);
    }
    @Test
    public void selectChild(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        CategoryMapper mapper = ctx.getBean("categoryMapper", CategoryMapper.class);
        List<Category> categories = mapper.selectChildAndCountByParentId(1);
        System.out.println("categories = " + categories);
    }
    @Test
    public void selectByChildId(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        CategoryMapper mapper = ctx.getBean("categoryMapper", CategoryMapper.class);
        Integer id = mapper.selectParentIdByChildId(8);
        System.out.println("id = " + id);
    }
}