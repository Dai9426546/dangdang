package com.baizhi.test;

import com.baizhi.entity.Book;
import com.baizhi.mapper.BookMapper;
import com.baizhi.service.BookService;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class TestBook {
    @Test
    public void selectHotSale(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        BookMapper bookMapper = ctx.getBean("bookMapper", BookMapper.class);
        List<Book> books = bookMapper.selectHotSale();
        for (Book book : books) {
            System.out.println(book);
        }
//        List<Book> books1 = bookMapper.selecctGrounding();
//        for (Book book : books1) {
//            System.out.println(book);
//        }
//        List<Book> books2 = bookMapper.selectInventory();
//        for (Book book : books2) {
//            System.out.println(book);
//        }
//        Book book = bookMapper.selectByBookId(1);
//        System.out.println("book = " + book);
    }
    @Test
    public void query(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        BookService service = ctx.getBean("bookServiceImpl", BookService.class);
        List<Book> books = service.queryHotSale();
        System.out.println("books = " + books);
    }
    @Test
    public void page(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        BookService service = ctx.getBean("bookServiceImpl", BookService.class);
        PageInfo<Book> bookPageInfo = service.queryByPageAndParentId(1, 1, 5);
        List<Book> list = bookPageInfo.getList();
        System.out.println("list = " + list);
        int pages = bookPageInfo.getPages();
        System.out.println("pages = " + pages);
        PageInfo<Book> bookPageInfo1 = service.queryByPageAndChildIdId(8, 1, 5);
        System.out.println("bookPageInfo1 = " + bookPageInfo1);
    }
}
