<%@page contentType="text/html;charset=utf-8" isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>收获地址管理 - 当当网</title>
		<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/css/page_bottom.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<%@include file="../common/head1.jsp"%>
		<div class="login_step">
			我的收货地址：
		</div>
		<div class="fill_message">

			<table class="tab_login">
				<tr>
					<td valign="top" class="w1" style="text-align: left">
						<b>序号</b>
					</td>
					<td valign="top" class="w1" style="text-align: left">
						<b>收货地址编号</b>
					</td>
					<td valign="top" class="w1" style="text-align: left">
						<b>收货人姓名</b>
					</td>
					<td valign="top" class="w1" style="text-align: left">
						<b>收货人地址</b>
					</td>
					<td valign="top" class="w1" style="text-align: left">
						<b>收货人联系方式</b>
					</td>
					<td valign="top" class="w1" style="text-align: left">
						<b>收货地址状态</b>
					</td>
					<td valign="top" class="w1" style="text-align: left">
						<b>操作</b>
					</td>
				</tr>

				<!-- 收货地址开始 -->
				<c:forEach items="${requestScope.addresses}" var="addr" varStatus="status">
					<tr>
						<td valign="top">
<%--							${status.count}--%>
						</td>
						<td valign="top">
							${addr.id}
						</td>
						<td valign="top">
							${addr.name}
						</td>
						<td valign="top">
							${addr.address}
						</td>
						<td valign="top">
							${addr.mobile}
						</td>
						<td valign="top">
							<c:if test="${addr.select_state==1}">
								默认收货地址
							</c:if>
							<c:if test="${addr.select_state==0}">
								<a href="${pageContext.request.contextPath}/address/updateState?aid=${addr.id}">设置为默认地址</a>
							</c:if>
						</td>
						<td valign="top">
							<c:if test="${addr.select_state==1}">
								删除
							</c:if>
							<c:if test="${addr.select_state==0}">
								<a  href="${pageContext.request.contextPath}/address/deleteAddress?aid=${addr.id}">删除</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>

				<!--收货地址结束 -->
			</table>
			
			<div style="margin: 50px;text-align: center;">
				<a href="${pageContext.request.contextPath}/address/add_address.jsp">添加新的收货地址</a>
				<a href="${pageContext.request.contextPath}/book/showMain">
					<input 	class="button_1"  type="button" value="取消" />
				</a>  
	    	</div>
		</div>
		<%@include file="../common/foot1.jsp"%>
	</body>
</html> 
