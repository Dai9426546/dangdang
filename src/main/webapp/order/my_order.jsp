<%@page pageEncoding="UTF-8" contentType="text/html;UTF-8" isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>订单</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/dingdan.css" type="text/css"/>
	</head>
	<body>
		<c:forEach var="order" items="${requestScope.orders}" >
		<table class="table-head table-mod">
			<tbody>
				<tr>
					<th width="331px">宝贝</th>
					<th>单价</th>
					<th>数量</th>
					<th>实付款</th>
					<th>交易状态</th>
				</tr>
			</tbody>

				<div class="dom">
					<label>
						<span class="time">
							<input type="checkbox"/>
							<span class="time"><fmt:formatDate value="${order.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></span>
						</span>
						<span>
							<span>订单号</span>
							<span>:</span>
							<span>${order.id}</span>
						</span>
					</label>
				</div>
				<table class="tb">
					<c:forEach items="${order.items}" var="orderItem" varStatus="sta">
						<tr>
							<td width="330px">
								<div style="display: block;">
									<img src="${pageContext.request.contextPath}/productImages/${orderItem.book.product_image}" style="float: left;"/>
									<p style="width: 230px; margin-left:10px; float: left;">
										<a href="javascript:void(0)"><span class="sp">${orderItem.book.book_name}</span></a>
									</p>
								</div>
							</td>
							<td width="238px" align="center">
								<div>
									<p>$${orderItem.book.dangPrice}</p>
								</div>
							</td>

							<td width="238px" align="center">${orderItem.count}本</td>

							<c:if test="${sta.index==0}">
								<td class="boder" rowspan="${order.items.size()}">$${order.sumMoney}</td>
								<td class="boder" width="239px" rowspan="${order.items.size()}">
									<div>
										<c:if test="${order.orderState==0}">
											<a href="${pageContext.request.contextPath}/order/payMoney?out_trade_no=10&order_id=${order.id}&total_amount=${order.sumMoney}">去付款</a>
										</c:if>
										<c:if test="${order.orderState==1}">
											<p>交易成功</p>
										</c:if>
									</div>
								</td>
							</c:if>
						</tr>
					</c:forEach>
				</table>
			</c:forEach>
		</table>

		<div style="margin: 50px;text-align: center;">
			<a href="javascript:history.go(-1);">
			<input 	class="button_1"  type="button" value="取消" /></a>
	    </div>
	</body>
</html>