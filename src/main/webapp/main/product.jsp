<%@page contentType="text/html;charset=utf-8" isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
ul li {
	text-decoration: none;
	list-style-type: none;
	line-height: 20px;
}

body {
	font-family: 宋体, Arial, Helvetica, sans-serif;
	font-size: 12px;
	background: #769b72 url(${pageContext.request.contextPath}/images/booksaleimg/bg_grad.gif) top center
		no-repeat;
	cursor: default;
	color: #404040;
	margin: 0;
	padding: 0;
}
</style>
</head>
<body>
	<div style="width: 60%;margin:20px auto;">
		<div style="width:100%;float: left;">
			<h1>
				
			</h1>
			<span><font color="#cccccc">丛书名：${book.book_name}</font></span>
			<hr />
			<div>
				<div style="float: left;width:20%;">
					<img src="${pageContext.request.contextPath}/productImages/${book.product_image}"  border=0 />
				</div>
				<div style="float: left;width:80%">
					<ul class="info">
						<li>作&nbsp;者：${book.author}</li>
						<li>出版社：人民出版社</li>
						<li style="float:left;width:50%;">出版时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${book.groundingDate}"/></li>
						<li style="float:left;width:50%;">字数：1000000</li>
						<li style="float:clear;"></li>
						<li style="float:left;width:50%;">版次：5</li>
						<li style="float:left;width:50%;">页数：500</li>
						<li style="float:clear;"></li>
						<li style="float:left;width:50%;">印刷时间：2021-10-22</li>
						<li style="float:left;width:50%;">开本：	203</li>
						<li style="float:clear;"></li>
						<li style="float:left;width:50%;">印次：222</li>
						<li style="float:left;width:50%;">纸张：33344</li>
						<li style="float:clear;"></li>
						<li style="float:left;width:50%;">ISBN：3335q422</li>
						<li style="float:left;width:50%;">包装：2222</li>
						<li>
							<div class='your_position'>
								您现在的位置:&nbsp; <a href='${pageContext.request.contextPath}/book/showMain'>当当图书</a> &gt;&gt;
								<font style='color: #cc3300'> 
									<strong> 
									</strong>
								</font>
							</div>
						</li>
						<li>定价：￥&nbsp;&nbsp;${book.book_price}
							当当价：￥&nbsp;&nbsp;${book.dangPrice} 节省：￥${book.book_price-book.dangPrice}</li>
						<li>
							<%--<a href="javascript:void(0)">--%>
							<a href="${pageContext.request.contextPath}/cart/putCart?bookId=${book.book_id}">
							<img src='${pageContext.request.contextPath}/images/buttom_goumai.gif' class="abc"
								onclick="" />
						</a></li>
					</ul>
				</div>
				<div style="float: clear;"></div>
			</div>
		</div>
	</div>
</body>
</html>