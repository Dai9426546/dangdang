package com.baizhi.controller;

import com.baizhi.entity.Book;
import com.baizhi.entity.Cart;
import com.baizhi.entity.CartItem;
import com.baizhi.service.BookService;
import com.baizhi.util.CartUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private BookService service;
    @RequestMapping("/putCart")
    public String addBook(Integer bookId, HttpSession session){
        Book book = service.queryBookById(bookId);
        Cart cart = (Cart)session.getAttribute("cart");
        if (cart == null) {
            //第一次购买
            cart=new Cart();
            CartItem item = new CartItem();
            item.setBook(book);
            item.setAmount(1);
            Map<Integer,CartItem> buyProducts=new HashMap<Integer,CartItem>();
            Map<Integer,CartItem> removeProducts=new HashMap<Integer,CartItem>();
            buyProducts.put(bookId,item);
            cart.setBuyProducts(buyProducts);
            Double saveMoney = CartUtil.saveMoney(cart);
            Double sumMoney = CartUtil.sumMoney(cart);
            cart.setSaveMoney(saveMoney);
            cart.setSumMoney(sumMoney);
            cart.setRemoveProducts(removeProducts);
            session.setAttribute("cart",cart);
        }else {
            Map<Integer, CartItem> buyProducts = cart.getBuyProducts();
            if (buyProducts.containsKey(bookId)){
                CartItem item = buyProducts.get(bookId);
                item.setAmount(item.getAmount()+1);
            }else {
                //是第一次购买
                CartItem newCartItem=new CartItem();
                newCartItem.setBook(book);
                newCartItem.setAmount(1);
                buyProducts.put(bookId,newCartItem);
            }
            Map<Integer, CartItem> removeProducts = cart.getRemoveProducts();
            if (removeProducts != null) {
                if (removeProducts.containsKey(bookId)){
                    removeProducts.remove(bookId);
                }
            }
            cart.setBuyProducts(buyProducts);
            Double saveMoney = CartUtil.saveMoney(cart);
            Double sumMoney = CartUtil.sumMoney(cart);
            cart.setRemoveProducts(removeProducts);
            cart.setSumMoney(sumMoney);
            cart.setSaveMoney(saveMoney);
        }
        return "redirect:/cart/cart_list.jsp";
    }
    @RequestMapping("/deleteCart")
    public String deleteCart(Integer bookId, HttpSession session){
        Cart cart = (Cart) session.getAttribute("cart");
        Map<Integer, CartItem> buyProducts = cart.getBuyProducts();
        Map<Integer, CartItem> removeProducts = cart.getRemoveProducts();
        CartItem item = buyProducts.get(bookId);
        buyProducts.remove(bookId);
        removeProducts.put(bookId,item);
        Double sumMoney = CartUtil.sumMoney(cart);
        Double saveMoney = CartUtil.saveMoney(cart);
        cart.setSumMoney(sumMoney);
        cart.setSaveMoney(saveMoney);
        return "redirect:/cart/cart_list.jsp";
    }
    @RequestMapping("/updateCartCount")
    public String updateCartCount(Integer bookId, HttpSession session,Integer count){
        Cart cart = (Cart) session.getAttribute("cart");
        Map<Integer, CartItem> buyProducts = cart.getBuyProducts();
        CartItem item = buyProducts.get(bookId);
        item.setAmount(count);
        Double saveMoney = CartUtil.saveMoney(cart);
        Double sumMoney = CartUtil.sumMoney(cart);
        cart.setSaveMoney(saveMoney);
        cart.setSumMoney(sumMoney);
        return "redirect:/cart/cart_list.jsp";
    }
}
