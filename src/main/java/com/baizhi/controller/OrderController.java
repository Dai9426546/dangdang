package com.baizhi.controller;

import com.baizhi.entity.*;
import com.baizhi.service.AddressService;
import com.baizhi.service.OrderItemService;
import com.baizhi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private OrderService orderService;
    @RequestMapping("/addOrder")
    public String addOrder(Integer addressId, HttpSession session){
        //将当前购物车里面的图书存放进订单并添加到数据库
        Order order= new Order();
        //获取订单的相关属性并存入数据库
        Cart cart = (Cart) session.getAttribute("cart");
        User user = (User) session.getAttribute("flag");
        Address address = addressService.queryAddressById(addressId);
        order.setUser(user);
        order.setAddress(address);
        order.setSumMoney(cart.getSumMoney());
        orderService.addOrder(order);
        session.setAttribute("order",order);
        //生成多个订单详情，并存入数据库
        Map<Integer, CartItem> buyProducts = cart.getBuyProducts();
        Collection<CartItem> values = buyProducts.values();
        List<OrderItem> list=new ArrayList<>();
        for (CartItem  cartItem: values) {
            OrderItem orderItem = new OrderItem();
            orderItem.setBook(cartItem.getBook());
            orderItem.setCount(cartItem.getAmount());
            Double itemSumMoney=cartItem.getBook().getDangPrice()*cartItem.getAmount();
            orderItem.setAmount(itemSumMoney);
            orderItem.setOrder(order);
            list.add(orderItem);
        }
        //调用业务层方法将list存入数据库
        orderItemService.addOrderItem(list);
        session.removeAttribute("cart");
        return "forward:/order/order_ok.jsp";
    }
    @RequestMapping("/showOrdersByUserId")
    public String showOrdersByUserId(HttpSession session, Model model){
        //调用service方法查询当前用户下的所有订单
        User user = (User) session.getAttribute("flag");
        List<Order> orders = orderService.queryAllOrderById(user.getId());
        model.addAttribute("orders",orders);
        return "forward:/order/my_order.jsp";
    }
    @RequestMapping("/payMoney")
    public String payMoney(Integer order_id){
        //修改指定订单的支付状态
        orderService.updateOrderStateById(order_id);
        return "redirect:/order/pay_ok.jsp";
    }
}
