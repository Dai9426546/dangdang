package com.baizhi.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.baizhi.entity.User;
import com.baizhi.service.UserService;
import com.baizhi.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.soap.SAAJResult;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
@RequestMapping("/user")
@SessionAttributes(value = "flag")
public class UserController {
    @Autowired
    private UserService service;
    @RequestMapping("/validateCode")
    public void validateCode(HttpSession session, HttpServletResponse response) throws IOException {
        //1.设置验证码图片的宽和高
        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(100, 50);
        //2.获取验证码中保存的内容
        String code = captcha.getCode();
        //3.将验证码内容存入session作用域
        session.setAttribute("validateCode",code);
        //4.获取字节输出流,将验证码图片写回页面
        ServletOutputStream os = response.getOutputStream();
        captcha.write(os);
    }
   @RequestMapping("/addUser")
    public String addUser(User user, String number,HttpSession session) throws UnsupportedEncodingException {
       //获取储存在session中的验证码
       String validateCode = (String)session.getAttribute("validateCode");
       //判断两次验证码是否一致
       if (validateCode.equals(number)){
            //判断邮箱是否被占用
           User user1 = service.queryUserByEmail(user.getEmail());
           if (user1!=null){
               return "redirect:/user/register_form.jsp?errorMessage="+ URLEncoder.encode("邮箱被占用","UTF-8");
           }else {
               service.register(user);
               String content = "请<a href='http://localhost:9999/dangdang/user/active?email="+user.getEmail()+"'>点击</a>激活账户";
               MailUtil.sendSimpleMail(user.getEmail(),"激活当当账号",content);
               session.setAttribute("state",user);
               return "redirect:/user/verify_form.jsp";
           }
       }else{
           return "redirect:/user/register_form.jsp?errorMessage="+ URLEncoder.encode("验证码不一致","UTF-8");

       }
   }
   @RequestMapping("/active")
    public String active(HttpSession session,String email){
       User user = (User)session.getAttribute("state");
       //调用激活方法
       service.active(email);
       return "/user/register_ok.jsp";
   }
   @RequestMapping("/login")
    public String login(String email, String password, Model model){
       User user = service.queryUserByEmailAndPwd(email, password);
       if (user == null) {
           return "redirect:/user/login_form.jsp?errorMessage=账户或密码错误";
       }else {
           if (user.getState()==1){
               model.addAttribute("flag",user);
               return "redirect:/book/showMain";
           }else {
               return "redirect:/user/verify_form.jsp";
           }

       }
   }
   @RequestMapping("/loginOut")
    public String loginOut(SessionStatus status){
       if (!status.isComplete()) {
           status.setComplete();
       }
       return "redirect:/book/showMain";
    }

}
