package com.baizhi.controller;

import com.baizhi.entity.Address;
import com.baizhi.entity.User;
import com.baizhi.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/address")
public class AddressController {
    @Autowired
    private AddressService service;
    @RequestMapping("/showAddresses")
    public String showAddresses(HttpSession session, Model model){
        //调用查询当前用户id所有未删除的地址
        User user = (User)session.getAttribute("flag");
        List<Address> addresses = service.queryAllAddressById(user.getId());
        model.addAttribute("addresses",addresses);
        return "forward:/address/my_address.jsp";
    }
    @RequestMapping("/addAddress")
    public String addAddress(Address address,HttpSession session){
        User user = (User)session.getAttribute("flag");
        address.setUser(user);
        service.addUserAddressById(address);
        return "redirect:/address/showAddresses";
    }
    @RequestMapping("/deleteAddress")
    public String deleteAddress(Integer aid,HttpSession session){
        System.out.println("aid = " + aid);
        User user = (User)session.getAttribute("flag");
        service.updateDeleteStateToZero(user.getId(),aid);
        return "redirect:/address/showAddresses";
    }
    @RequestMapping("/updateState")
    public String updateState(Integer aid,HttpSession session){
        //将这个aid设为默认收获地址
        User user = (User)session.getAttribute("flag");
        //查询当前用户存在默认地址不
        Address address = service.querySelectAddressById(user.getId());
        //不存在时直接将aid设置为默认地址
        if (address == null) {
            service.updateSelectStateToOneById(user.getId(),aid);
        }else {
            //将原来的默认地址设置为非默认地址
            service.updateSelectStateToZeroById(user.getId(),address.getId());
            //将现在的aid设置为默认地址
            service.updateSelectStateToOneById(user.getId(),aid);
        }
        return "redirect:/address/showAddresses";
    }
    @RequestMapping("/selectAllAddressById")
    public String selectAllAddressById(HttpSession session){
        //获取当前用户下所有的地址并存入session
        User user = (User) session.getAttribute("flag");
        List<Address> addresses = service.queryAllAddressById(user.getId());
        session.setAttribute("addresses",addresses);
        return "redirect:/order/address_form.jsp";
    }
}
