package com.baizhi.controller;

import com.baizhi.entity.Book;
import com.baizhi.entity.Category;
import com.baizhi.service.BookService;
import com.baizhi.service.CategoryService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/book")
@Controller
public class BookController {
    @Autowired
    private BookService bookService;
    @Autowired
    private CategoryService categoryService;
    @RequestMapping("/showMain")
    public String showMain(Model model){
        List<Book> saleHotBooks = bookService.queryHotSale();//热销
        List<Book> newReleasesBooks = bookService.queryGrounding();//最新上架
        List<Book> recommendBooks = bookService.queryInventory();//编辑推荐
        List<Book> newHotBooks=recommendBooks;//右侧展示
        model.addAttribute("saleHotBooks",saleHotBooks);
        model.addAttribute("newReleasesBooks",newReleasesBooks);
        model.addAttribute("recommendBooks",recommendBooks);
        model.addAttribute("newHotBooks",newHotBooks);
        List<Category> categories = categoryService.queryAllCateGory();
        model.addAttribute("categories",categories);
        return "forward:/main/main.jsp";
    }
    @RequestMapping("/selectPageByLevelOne")
    public String selectPageByLevelOne(Model model,Integer id,Integer pageNum,Integer pageSize){
        //查询父类别名
        String parentName = categoryService.queryCateNameByParentId(id);
        //查询子类别信息及对应图书数量
        List<Category> categories = categoryService.queryChildAndCountByParentId(id);
        //查询父类别下图书数量
        Integer count = categoryService.queryCountByParentId(id);
        //查询当前页的数据
        PageInfo<Book> page = bookService.queryByPageAndParentId(id, pageNum, pageSize);
        Category category = new Category();
        category.setId(id);
        category.setName(parentName);
        category.setCount(count);
        category.setCategories(categories);
        model.addAttribute("category",category);
        model.addAttribute("page",page);
        model.addAttribute("level","one");
        return "forward:/main/book_list.jsp";
    }
    @RequestMapping("/selectPageByLevelTwo")
    public String selectPageByLevelTwo(Integer id,Model model,Integer pageNum,Integer pageSize){
        //根据子类id查询父类id
        Integer parentId = categoryService.queryParentIdByChildId(id);
        //查询父类别名
        String parentName = categoryService.queryCateNameByParentId(parentId);
        //查询子类别信息及对应图书数量
        List<Category> categories = categoryService.queryChildAndCountByParentId(parentId);
        //查询父类别下图书数量
        Integer count = categoryService.queryCountByParentId(parentId);
        //根据子类id查当前页图书
        PageInfo<Book> page = bookService.queryByPageAndChildIdId(id, pageNum, pageSize);
        Category category = new Category();
        category.setId(id);
        category.setName(parentName);
        category.setCount(count);
        category.setCategories(categories);
        model.addAttribute("category",category);
        model.addAttribute("page",page);
        model.addAttribute("level","two");
        return "forward:/main/book_list.jsp";
    }
    @RequestMapping("/showBookById")
    public String showBookById(Integer id,Model model){
        //根据图书id查询图书
        Book book = bookService.queryBookById(id);
        model.addAttribute("book",book);
        return "forward:/main/product.jsp";
    }
}
