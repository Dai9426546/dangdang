package com.baizhi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    private Integer id;
    private Date orderTime;
    private Integer orderState;
    private User user;
    private Address address;
    private Double sumMoney;
    private List<OrderItem> items;
}
