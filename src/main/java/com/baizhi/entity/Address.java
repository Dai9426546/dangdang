package com.baizhi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    private Integer id;
    private String name;
    private String address;
    private String mobile;
    private User user;
    private Integer delete_state;
    private Integer select_state;
}
