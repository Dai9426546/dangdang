package com.baizhi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    Map<Integer,CartItem> buyProducts;
    Map<Integer,CartItem> removeProducts;
    private Double saveMoney;
    private Double sumMoney;
}
