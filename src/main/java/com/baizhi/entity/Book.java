package com.baizhi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book implements Serializable {
    private Integer book_id;
    private Integer cid;//种类
    private String book_name;
    private Double book_price;
    private Date groundingDate;//上架时间
    private Integer inventory;
    private String author;
    private Integer saleNum;//销量
    private String messages;
    private Double dangPrice;//当当网价格
    private String product_image;
}
