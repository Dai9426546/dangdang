package com.baizhi.util;

import com.baizhi.entity.Book;
import com.baizhi.entity.Cart;
import com.baizhi.entity.CartItem;

import java.util.Collection;
import java.util.Map;

public class CartUtil {

    public static Double saveMoney(Cart cart){
        Double saveMoney = 0.0;
        Map<Integer, CartItem> buyProducts = cart.getBuyProducts();
        Collection<CartItem> cartItems = buyProducts.values();
        for (CartItem cartItem : cartItems) {
            Book book = cartItem.getBook();
            saveMoney=saveMoney+(book.getBook_price()- book.getDangPrice())*cartItem.getAmount();
        }
        return saveMoney;
    }
    public static Double sumMoney(Cart cart){
        Double sumMoney = 0.0;
        Map<Integer, CartItem> buyProducts = cart.getBuyProducts();
        Collection<CartItem> cartItems = buyProducts.values();
        for (CartItem cartItem : cartItems) {
            Book book = cartItem.getBook();
            sumMoney=sumMoney+book.getDangPrice() * cartItem.getAmount();
        }
        return sumMoney;
        
    }
}
