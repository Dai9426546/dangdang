package com.baizhi.mapper;

import com.baizhi.entity.Category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryMapper {
    //查询所有一级类别及其下属类别
    List<Category> selectAllCategory();
    //根据一级id查询一级类别名
    String selectCateNameByParentId(@Param("id") Integer id);
    //查询一级类别下图书数量
    Integer selectCountByParentId(@Param("id") Integer id);
    //根据一级类别id查询下面的二级类别信息以及对应图书数
    List<Category> selectChildAndCountByParentId(@Param("id") Integer id);
    //
    Integer selectParentIdByChildId(@Param("id") Integer id);
}
