package com.baizhi.mapper;

import com.baizhi.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    //添加订单
    void insertOrder(@Param("order") Order order);
    List<Order> selectAllOrderById(@Param("id") Integer user_id);
    void updateOrderStateById(@Param("id") Integer id);
}
