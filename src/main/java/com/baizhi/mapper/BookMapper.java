package com.baizhi.mapper;

import com.baizhi.entity.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    //根据库存查
    List<Book> selectInventory();
    //根据热销查询
    List<Book> selectHotSale();
    //根据出版时间查
    List<Book> selecctGrounding();
    //根据父类型id查
    List<Book> selectByPageAndParentId(@Param("id")Integer id);
    //根据子类型id查
    List<Book> selectByPageAndChildIdId(@Param("id") Integer id);
    //根据图书id查询图书
    Book selectByBookId(@Param("id")Integer id);
}
