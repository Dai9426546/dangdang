package com.baizhi.mapper;

import com.baizhi.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    User selectUserByEmail(@Param("email") String email);
    void insert(@Param("user") User user);
    void activeUser(@Param("email") String email);
    User selectUserByEmailAndPwd(@Param("email") String email,@Param("pwd") String pwd);
}
