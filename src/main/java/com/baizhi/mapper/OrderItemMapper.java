package com.baizhi.mapper;

import com.baizhi.entity.Order;
import com.baizhi.entity.OrderItem;
import org.apache.ibatis.annotations.Param;

public interface OrderItemMapper {
    void insertOrderItem(@Param("item") OrderItem item);
}
