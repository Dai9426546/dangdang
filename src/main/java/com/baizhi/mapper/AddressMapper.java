package com.baizhi.mapper;

import com.baizhi.entity.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {
    //为当前用户添加地址
    void insertAddressById(@Param("address") Address address);
    //根据用户id查询未删除地址
    List<Address> selectAllAddressById(@Param("u_id")Integer id);
    //通过用户id将当前地址修改为默认地址
    void updateSelectStateToOneById(@Param("u_id")Integer id,@Param("a_id")Integer address_id);
    //通过用户id将当前地址修改为非默认地址
    void updateSelectStateToZeroById(@Param("u_id")Integer id,@Param("a_id")Integer address_id);
    //查询当前用户是否有默认地址
    Address selectSelectAddressById(@Param("u_id")Integer id);
    //修改当前用户delete_state为0
    void updateDeleteStateToZero(@Param("u_id")Integer id,@Param("a_id")Integer address_id);
    //根据地址id查询地址
    Address selectAddressById(@Param("aid")Integer aid);
}
