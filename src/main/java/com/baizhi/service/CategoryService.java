package com.baizhi.service;

import com.baizhi.entity.Category;

import java.util.List;

public interface CategoryService {
    //查询一级类别一级下属类别
    List<Category> queryAllCateGory();
    //根据一级id查询一级类别名
    String queryCateNameByParentId(Integer id);
    //查询一级类别下图书数量
    Integer queryCountByParentId(Integer id);
    //根据一级类别id查询下面的二级类别信息以及对应图书数
    List<Category> queryChildAndCountByParentId(Integer id);
    //根据子类id查询父类id
    Integer queryParentIdByChildId(Integer id);
}
