package com.baizhi.service.impl;

import com.baizhi.entity.Order;
import com.baizhi.mapper.OrderMapper;
import com.baizhi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper mapper;
    @Override
    public void addOrder(Order order) {
        mapper.insertOrder(order);
    }

    @Override
    public List<Order> queryAllOrderById(Integer user_id) {
        return mapper.selectAllOrderById(user_id);
    }

    @Override
    public void updateOrderStateById(Integer id) {
        mapper.updateOrderStateById(id);
    }
}
