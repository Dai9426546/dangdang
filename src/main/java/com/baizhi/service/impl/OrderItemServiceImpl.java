package com.baizhi.service.impl;

import com.baizhi.entity.OrderItem;
import com.baizhi.mapper.OrderItemMapper;
import com.baizhi.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemServiceImpl implements OrderItemService {
    @Autowired
    private OrderItemMapper mapper;
    @Override
    public void addOrderItem(List<OrderItem> list) {
        for (OrderItem item : list) {
            mapper.insertOrderItem(item);
        }
    }
}
