package com.baizhi.service.impl;

import com.baizhi.entity.User;
import com.baizhi.mapper.UserMapper;
import com.baizhi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper mapper;
    @Override
    public User queryUserByEmail(String email) {
        return mapper.selectUserByEmail(email);
    }

    @Override
    public void register(User user) {
        mapper.insert(user);
    }

    @Override
    public void active(String email) {
        mapper.activeUser(email);
    }

    @Override
    public User queryUserByEmailAndPwd(String email, String pwd) {
        return mapper.selectUserByEmailAndPwd(email,pwd);
    }
}
