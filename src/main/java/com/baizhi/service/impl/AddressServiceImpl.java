package com.baizhi.service.impl;

import com.baizhi.entity.Address;
import com.baizhi.mapper.AddressMapper;
import com.baizhi.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressMapper mapper;

    @Override
    public void addUserAddressById(Address address) {
        mapper.insertAddressById(address);
    }

    @Override
    public List<Address> queryAllAddressById(Integer id) {
        return mapper.selectAllAddressById(id);
    }

    @Override
    public void updateSelectStateToOneById(Integer id, Integer address_id) {
        mapper.updateSelectStateToOneById(id,address_id);
    }

    @Override
    public void updateSelectStateToZeroById(Integer id, Integer address_id) {
        mapper.updateSelectStateToZeroById(id, address_id);
    }

    @Override
    public Address querySelectAddressById(Integer id) {
        return mapper.selectSelectAddressById(id);
    }

    @Override
    public void updateDeleteStateToZero(Integer id, Integer address_id) {
        mapper.updateDeleteStateToZero(id,address_id);
    }

    @Override
    public Address queryAddressById(Integer id) {
        return mapper.selectAddressById(id);
    }

}
