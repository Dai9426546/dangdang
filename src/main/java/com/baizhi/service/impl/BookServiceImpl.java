package com.baizhi.service.impl;

import com.baizhi.entity.Book;
import com.baizhi.mapper.BookMapper;
import com.baizhi.service.BookService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper mapper;

    @Override
    public List<Book> queryInventory() {
        return mapper.selectInventory();
    }

    @Override
    public List<Book> queryHotSale() {
        return mapper.selectHotSale();
    }

    @Override
    public List<Book> queryGrounding() {
        return mapper.selecctGrounding();
    }

    @Override
    public PageInfo<Book> queryByPageAndParentId(Integer id, Integer pageNum, Integer pageSize) {
        return PageHelper.startPage(pageNum,pageSize).doSelectPageInfo(()->mapper.selectByPageAndParentId(id));
    }

    @Override
    public PageInfo<Book> queryByPageAndChildIdId(Integer id, Integer pageNum, Integer pageSize) {
        return PageHelper.startPage(pageNum,pageSize).doSelectPageInfo(()->mapper.selectByPageAndChildIdId(id));
    }

    @Override
    public Book queryBookById(Integer id) {
        return mapper.selectByBookId(id);
    }
}
