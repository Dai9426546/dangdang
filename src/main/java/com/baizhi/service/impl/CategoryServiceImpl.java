package com.baizhi.service.impl;

import com.baizhi.entity.Category;
import com.baizhi.mapper.CategoryMapper;
import com.baizhi.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryMapper mapper;
    @Override
    public List<Category> queryAllCateGory() {
        return mapper.selectAllCategory();
    }

    @Override
    public String queryCateNameByParentId(Integer id) {
        return mapper.selectCateNameByParentId(id);
    }

    @Override
    public Integer queryCountByParentId(Integer id) {
        return mapper.selectCountByParentId(id);
    }

    @Override
    public List<Category> queryChildAndCountByParentId(Integer id) {
        return mapper.selectChildAndCountByParentId(id);
    }

    @Override
    public Integer queryParentIdByChildId(Integer id) {
        return mapper.selectParentIdByChildId(id);
    }
}
