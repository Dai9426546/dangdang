package com.baizhi.service;

import com.baizhi.entity.Book;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookService {
    List<Book> queryInventory();
    List<Book> queryHotSale();
    List<Book> queryGrounding();
    //根据父类型id查
    PageInfo<Book> queryByPageAndParentId(Integer id, Integer pageNum, Integer pageSize);
    //根据子类型id查
    PageInfo<Book> queryByPageAndChildIdId(Integer id,Integer pageNum,Integer pageSize);
    //根据图书id查图书
    Book queryBookById(Integer id);
}
