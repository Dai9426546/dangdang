package com.baizhi.service;

import com.baizhi.entity.Order;

import java.util.List;

public interface OrderService {
    //添加订单
    void addOrder(Order order);
    List<Order> queryAllOrderById(Integer user_id);
    void updateOrderStateById(Integer id);
}
