package com.baizhi.service;

import com.baizhi.entity.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressService {
    //为当前用户添加地址
    void addUserAddressById(Address address);
    //根据用户id查询未删除地址
    List<Address> queryAllAddressById(Integer id);
    //通过用户id将当前地址修改为默认地址
    void updateSelectStateToOneById(Integer id,Integer address_id);
    //通过用户id将当前地址修改为非默认地址
    void updateSelectStateToZeroById(Integer id,Integer address_id);
    //查询当前用户是否有默认地址
    Address querySelectAddressById(Integer id);
    //修改当前用户delete_state为0
    void updateDeleteStateToZero(Integer id,Integer address_id);
    //根据地址id查询地址
    Address queryAddressById(Integer id);
}
