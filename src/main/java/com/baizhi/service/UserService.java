package com.baizhi.service;

import com.baizhi.entity.User;

public interface UserService {
    User queryUserByEmail(String email);
    void register(User user);
    void active(String email);
    User queryUserByEmailAndPwd(String email,String pwd);
}
