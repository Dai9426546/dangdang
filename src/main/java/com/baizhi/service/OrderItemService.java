package com.baizhi.service;

import com.baizhi.entity.OrderItem;

import java.util.List;

public interface OrderItemService {
    void addOrderItem(List<OrderItem> list);
}
